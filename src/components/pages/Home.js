import React from 'react';
import img5 from './Images/img5.jpg'
import img2 from './Images/img2.jpg'
import img4 from './Images/img4.jpg'
import civil3 from './Images/civil3.jpeg'
import jrh2 from './Images/jrh2.jpg'
import Navigation from '../shared/Navigation'

const Home = () => {
    return(
<div>
  
  <table border={0} width="100%" cellPadding={10}>

    <tbody><tr> 
        <td width="30%" className="jrh"> <img src={civil3} alt="Civil Ing" width="100%" className="jrh-img" /></td>
        <td width="70%" colSpan={3}> <font size={40} color="black"> Jose Rolando Heredia</font></td>
        
      </tr>
      </tbody>
    </table>
    <Navigation/>
  <br /><br /><br /><br />
  <table>
    <tbody>
      <tr>
        <td width="8%" className="jrh"> <img src={jrh2} alt="jrh" width="100%" className="jrh-img" /></td>
        <td width="92%" colSpan={3}> <font size={40} color="black"><em>BIOGRAPHY</em></font></td>
      </tr>
    </tbody>
    </table>
  <br />
  <table border={0}>
    <tbody>
      <tr>
        <td bgcolor="#E6E9DF"><center><h3>ESPAÑOL</h3></center></td>
        <td bgcolor="#E6E9DF"><center><h3>ENGLISH</h3></center></td>
      </tr>
      <tr><td className="bio">
          Jose Rolando Heredia es un profesional de la ingenieria civil con 30 anos de experiencia, que a ejercidos deversos cargos en la industria, principalmente en Venezuela, en la universidad Santa Maria de Caracas obtiene el titulo de Ingeniero Civil en 1990, en el mismo ano cursa una especializacion en Vias de Comunicacion, posteriormente en el ano 2006 realiza una especializacion en Gerencia en el Instituto Venezolano de Gerencia; en su carrera laboral destaca su desempeno como vicepresidente ejecutivo en la compania "Class Light" empresa de la que formo parte 20 anos.
        </td>
        <td className="bio">
          Jose Rolando Heredia is a civil engineering professional with 30 years of experience, who has held various positions in the industry, mainly in Venezuela, at the University Santa Maria de Caracas obtained the title of Civil Engineer in 1990, in the same year he studied a specialization in Vias de Comunicacion, later in 2006 he made a specialization in Management at the Venezuelan Institute of Management; In his career, he stands out as an executive vice president in the company "Class Light", a company of which 20 years ago.
        </td>
      </tr>
    </tbody>
  </table><br /><br /><br /><br />
  
  <section className="portfolio">
    <h1>MY PORTFOLIO</h1>
    <div className="portfolio-container">
      <section className="portfolio-item">
      <img src={img5} alt="" width="400px" height="250" class="portfolio-img"/>
        <section className="portfolio-text">
          <h2>Skyscraper</h2>
          <p>PH The Grand Tower, Centro Seguros la Paz, Torres de Parque Central, <br />Parque Cristal
          </p>
        </section>
      </section>
      <section className="portfolio-item">
      <img src={img2} alt="" width="400px" height="250" class="portfolio-img"/>
        <section className="portfolio-text">
          <h2>Malls
          </h2>
          <p>CCCTamanaco, Centro San Ignacio, Centro Multi Plaza 
          </p>
        </section>
      </section>
      <section className="portfolio-item">
      <img src={img4} alt="" width="400px" height="250" class="portfolio-img"/>
        <section className="portfolio-text">
          <h2>Bridges</h2>
          <p>Viaducto Caracas-La Guaira, Puente Angostura (Ciudad Bolivar)
          </p>
        </section>
      </section>
    </div>
  </section>
  <br />
  <br />
  <br />
  <br />
  
</div>


    );
    }




export default Home;