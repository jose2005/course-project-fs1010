import React, { useState } from 'react';
import Navigation from '../shared/Navigation'
import { Form } from 'reactstrap';
//import { useForm } from 'react-hook-form' //to validate the form

const Contact = () => {
  

    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [phoneNumber, setPhoneNumber] = useState("")
    const [content, setContent] = useState("")

  const formSubmit = async event => {
    event.preventDefault()
    const response = await fetch('http://localhost:4000/contact_form/entries', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify({name, email, phoneNumber, content})
    })
    const payload = await response.json()
    if (response.status >= 400) {
        alert(`Sorry! Error: ${payload.message} for fields: ${payload.invalid.join(",")}`)
    } else {
        alert(`Perfect! Submission submitted with id: ${payload.id}`)
    }
}


/*I validated the form with the React hook form but I did not know 
  how to pass to the onsubmit event
  both functions (formSubmit and handleSubmit), 
  so I left the entire validation process with React hook form commented.*/


//const {register, errors, handleSubmit} = useForm();

/*const onSubmit = (data, event) => {
    console.log(data)
    event.target.reset()
    alert('All the data you entered is correct!!!')
    formSubmit()
}*/


      return(
<div>
  <table border={0} width="100%" cellPadding={10}>
    <tbody><tr> 
        <td width="25%"><font size={40} color="black">Jose Rolando Heredia</font> </td>
        <td width="75%" colSpan={3}> <font size={40} color="blue"> CONTACT</font></td>
      </tr>
    </tbody></table>
    <Navigation/> <br/><br/><br/>
  
    
    
  <div className="container" id="val">
  <Form id="myform" className="form" onSubmit={formSubmit}> {/* onSubmit={handlleSubmit(onSubmit)} */}
    <div className="form-header">
      <h1 className="form-title">C<span>ontact</span></h1>
      
    </div>

    <div className="input_field">
      <label htmlFor="name" className="form-label">Client Name</label>
      {/*{errors.name && errors.name.message}*/}
      <input 
      type="text" 
      id="name" 
      className="form-input" 
      placeholder="Name:"
      name="name"
      required value={name} onChange={e => setName(e.target.value)}
      /*ref={register({
        required: {
          value: true, 
          message: <p1>Name is requiere</p1>
            }, 
          maxLength: {
          value: 25, 
          message: <p1>no more than 25 characters!</p1>
            },
          minLength: {
          value: 2, 
          message: <p1>Minimum 2 characters</p1>
            }
      })}*/
      />
      
      
    </div>

    <div className="input_field">
      <label htmlFor="phone" className="form-label">Phone's Number</label>
      {/*{errors.phone && errors.phone.message}*/}
      <input 
      type="number" 
      id="phone" 
      className="form-input" 
      placeholder="Phone:" 
      name= "phone"
      required value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)}
      /*ref={register({
        required: {
          value: true, 
          message: <p1>Phone is requiere</p1>
            }, 
          maxLength: {
          value: 10, 
          message: <p1>Please Enter valid Phone Number e.g. 4166784520!</p1>
            },
          minLength: {
          value: 10, 
          message: <p1>Please Enter valid Phone Number e.g. 4166784520</p1>
            }
            
                            
      })}*/
      />
    </div>

    <div className="input_field">
      <label htmlFor="services" className="form-label">Email</label>
      {/*{errors.email && errors.email.message}*/}
      <input 
      type="text" 
      id="email" 
      className="form-input" 
      placeholder="Email:"
      name="email"
      required value={email} onChange={e => setEmail(e.target.value) }
      /*ref={register({
        required: {
            value: true, 
            message: <p1>Email is requiere</p1>
            }, 
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
              message: <p1>invalid email address e.g. f@a.ca</p1>
            }
    })}*/
      />
    </div>

    <div className="input_field">
      <label htmlFor="comment" className="form-label">Comment</label>
      {/*{errors.comment && errors.comment.message}*/}
      <textarea 
      id="message" 
      className="form-textarea" 
      placeholder="Comment" defaultValue={""} 
      name="comment"
      required value={content} onChange={e => setContent(e.target.value)}
      /*ref={register({
        required: {
            value: true, 
            message: <p1>Comment is requiere</p1>
            }, 
        maxLength: {
            value: 50, 
            message: <p1>no more than 50 characters!</p1>
            },
        minLength: {
            value: 6, 
            message: <p1>Minimum 6 characters</p1>
            }
    })}*/
      />
    </div>

    <div className="btn">
      <input type="submit" id="submit"  className="btn-submit" />
    </div>

  </Form> 
</div>


  <br />
  <br />
  <br />
  <br />
  
</div>
   );
    }

export default Contact;