import React from 'react';

import Navigation from '../shared/Navigation'

const About = () => {
    return(
  <div>
  <table border={0} width="100%" cellPadding={10}>
    <tbody><tr> 
        <td width="25%"><font size={40} color="black">Jose Rolando Heredia</font> </td>
        <td width="75%" colSpan={3}> <font size={40} color="blue"> ABOUT jrh</font></td>
      </tr>
    </tbody>
  </table> 
  <Navigation/>
  <div className="text"><br />
    <h3>RESUME</h3>
    <p>
      103 The Queensway, Unit #1203,<br /> Toronto ON M6S 5B3 <br />
      (647) 679-8830 <br />
      <a href="mailto:herediajoserolando@gmail.com">Email</a> <br /><br /><br />
      A Civil Engineer with 27+ years of experience. Committed to providing high quality service to every project, with focus on effectiveness and efficiency. Professional with extensive knowledge and experience of business management. <br /><br />
      PROFESSIONAL EXPERIENCE	<br />
      •	1987-1992 (5 years) <br />
      Resident engineer of different Works with the company COVIMETRO and MUPROCA, Caracas, Venezuela <br />
      •	1992-2011 (19 years) <br />
      Different positions in Corporación Industrial CLASS LIGHT, Caracas, Venezuela,<br /> 
      Telf.: +58212 7405408 a 10. <br /><br />
      1992-1996 Gerente de Operaciones <br />
      1996-2000 Director de Operaciones <br />
      2000-2011 Vicepresidente Ejecutivo <br /><br />
      Corporation companies: <br /><br />
      •	Class Light, S.A  (The second most important company in Venezuela of Billboard) <br />
      •	Metalex, S.A. (Construction Company) <br />
      •	Profit, S.A  (Importer of food and liquor) <br />
      •	Bodegas 1492, S.A ( Producer of rums “1492 El Descubrimiento” y “Sol y Luna”) <br /><br />
      •	2011-2016 (5 years) <br />
      Investor/Businessman in Ciudad de Panamá, Owner and operator of <br /><br />
      •	POSTNET MARBELLA (Multi Colors Services, s.a) <br />
      •	AWAKE CORP.  Event and youth tours company. <br />
      •	ESFERA DE LA BELLEZA, Beauty Salon “Me Salon” <br /><br />
      EDUCATION <br /><br />	
      UNIVERSIDAD SANTA MARIA	 CARACAS, VENEZUELA <br />
      CIVIL ENGINEERING 1990 <br />
      DEGREE OF ROAD ENGINEERING 1990 <br /><br />
      INSTITUTO VENEZOLANO DE GERENCIA <br />
      ADVANCE MANAGEMENT PROGRAM  2006
    </p><br />
    <br />
  </div>
</div>




);
    }




export default About;